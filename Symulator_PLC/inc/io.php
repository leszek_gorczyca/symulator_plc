<?php
$arch = 16;   // architektura 16 bitowa
$rozmiar = 4; // rozmiar obszarów
?>

<div id="PLC_io">
    <div class="PLC_inputs">
        <table>
            <thead>
                <tr>
                    <th style="text-align:center; width: 30px; ">IX</th>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <th style="text-align:center"><?= $j ?></th>
                    <?php }
                    ?>
                </tr>
            </thead>
            <?php for ($i = 0; $i < $rozmiar; $i++) { ?>
                <tr>
                    <td><?= $i ?></td>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <td>
                            <input type="checkbox" class="PLC_input" style="display: none;" id="IX<?= $i . '_' . $j ?>"><label for="IX<?= $i . '_' . $j ?>"></label>
                        </td>
                        <?php
                    }
                }
                ?>
            </tr>

        </table>
    </div>
    <div class="PLC_outputs">
        <table>
            <thead>
                <tr>
                    <th style="text-align:center; width: 30px; "">QX</th>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <th style="text-align:center"><?= $j ?></th>
                    <?php }
                    ?>
                </tr>
            </thead>
            <?php for ($i = 0; $i < $rozmiar; $i++) { ?>
                <tr>
                    <td><?= $i ?></td>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <td>
                            <input type="checkbox" class="PLC_output" style="display: none;" id="QX<?= $i . '_' . $j ?>" disabled><label for="QX<?= $i . '_' . $j ?>"></label>
                        </td>
                        <?php
                    }
                }
                ?>
            </tr>

        </table>
    </div>
    <div class="PLC_inputs mem">
        <table>
            <thead>
                <tr>
                    <th style="text-align:center; width: 30px; ">MX</th>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <th style="text-align:center"><?= $j ?></th>
                    <?php }
                    ?>
                    <th>DEC</th>
                </tr>
            </thead>
            <?php for ($i = 0; $i < $rozmiar; $i++) { ?>
                <tr>
                    <td><?= $i ?></td>
                    <?php for ($j = 0; $j < $arch; $j++) { ?>
                        <td>
                            <input type="checkbox" class="PLC_input" style="display: none;" id="MX<?= $i . '_' . $j ?>"><label for="MX<?= $i . '_' . $j ?>"></label>
                        </td>
                        <?php }
                    ?>
                        <td><input readonly="readonly" type="text" value="0" id="DEC_M_<?= $i ?>"></td>
                </tr> 
                <?php
            }
            ?>



        </table>
    </div>
</div>