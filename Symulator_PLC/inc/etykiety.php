<script>
    /**
     * Dodaje wiersz do tabli etykiet
     * 
     * @param {String} address Opcjonalnie Adres
     * @param {String} label Opcjonalnie Etykieta
     * @returns {undefined}
     */
    function PLC_etykiety_dodaj_wiersz(address, label) {
        // domyślne puste wartości dla argumentów jeśli nie zostały podane
        address = typeof address !== 'undefined' ? address : '';
        label = typeof label !== 'undefined' ? label : '';

        //wstawia nowy wiersz
        var new_row = $("#PLC_etykiety_tabela").append('<tr><td><button type="button" class="btn btn-default" onclick="PLC_etykiety_usun_wiersz(this)" ><span class="glyphicon glyphicon-remove-sign PLC_etykieta_remove" aria-hidden="true"></span></button></td><td><input value="'+address+'" type="text" class="form-control PLC_autocomplete_input PLC_etykiety_address"></td><td><input type="text" value="'+label+'" class="form-control PLC_etykiety_label"></td></tr>');
        //dodaje podpowiadanie adresow
        new_row.find('.PLC_autocomplete_input').autocomplete({
            source: PLC_mem_autocomplete
        });
    }
    /**
     * Usuwa wiersz z tabeli etykiet
     */
    function PLC_etykiety_usun_wiersz(e) {
        $(e).parents('tr').remove();
    }
    $(function () {
        // Uaktualnia liste etykiet po zamknieciu okienka
        $('#PLC_etykiety').on('hidden.bs.modal', function (e) {

            var adresses = $(".PLC_etykiety_address").map(function () {
                return $(this).val();
            }).get();
            var labels = $(".PLC_etykiety_label").map(function () {
                return $(this).val();
            }).get();
            PLC_labels.add_many(adresses, labels);
            
            //uaktualnia podpisy pod elementami
            $('.PLC_column').each(function(index) {
                $(this).empty();
                PLC_add_description($(this));
            });
        });
    });

</script>
<style>
    .ui-autocomplete {
        max-height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>

<div class="modal" tabindex="-1" role="dialog" id="PLC_etykiety">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Etykiety</h4>
            </div>
            <div class="modal-body ui-front">
                <table>
                    <thead>
                        <tr>
                            <td></td><td>Adres</td><td>Etykieta</td>
                        </tr>
                    </thead>
                    <tbody id="PLC_etykiety_tabela">

                    </tbody>
                    <tfoot>
                        <tr>
                            <td><button type="button" class="btn btn-default" onclick="PLC_etykiety_dodaj_wiersz()"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button></td><td><a href="javascript:PLC_etykiety_dodaj_wiersz()">Dodaj wiersz...</a></td><td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary">Zamknij</button>
            </div>
        </div>
    </div>
</div>