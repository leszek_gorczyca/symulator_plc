<nav class="navbar navbar-default">
    <a class="navbar-brand" href="#">Symulator PLC</a>

    <div class="btn-group" role="group">
        <button type="button" id="PLC_start" onclick="sim_start();" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Symulacja</button>
        <button type="button" disabled id="PLC_run" onclick="sim_run();" class="btn btn-default navbar-btn disabled"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></button>
        <button type="button" disabled id="PLC_pause" onclick="sim_pause();" class="btn btn-default navbar-btn disabled"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>
        <button type="button" disabled id="PLC_step" onclick="sim_step();" class="btn btn-default navbar-btn disabled"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></button>
        <button type="button" disabled id="PLC_run_once" onclick="sim_run_once();" class="btn btn-default navbar-btn disabled"><span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></button>
        <button type="button" disabled id="PLC_stop" onclick="sim_stop();" class="btn btn-default navbar-btn disabled"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span></button>
    </div>



    <ul class="nav navbar-nav">
        <li><a href="#" data-toggle="modal" data-target="#PLC_save">Zapisz</a></li>
        <li><a href="#" data-toggle="modal" data-target="#PLC_load">Wczytaj</a></li>
    </ul>
    <div class="navbar-form navbar-right">
        <div class="form-group">
            <label for="PLC-host">Połącz z Raspberry PI:</label>
            <input type="text" class="form-control" id="PLC-host" placeholder="Adres IP">
        </div>
        <button type="button" class="btn btn-default" onclick="PLC_raspberry.connect();">Połącz</button>
        <button type="button" class="btn btn-default navbar-btn" data-toggle="modal" data-target="#PLC_raspberry"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
    </div>

</nav>

<div class="btn-toolbar" role="toolbar" id="PLC_edit_ui">
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Dodaj szczebel
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#" onclick="PLC_add_rung_above($('.selected'));"><span class="fa fa-hand-o-up"></span> Powyżej</a></li>
            <li><a href="#" onclick="PLC_add_rung_below($('.selected'));"><span class="fa fa-hand-o-down"></span> Poniżej</a></li>
        </ul>


    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Dodaj wiersz
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#" onclick="PLC_add_row_above($('.selected'));"><span class="fa fa-hand-o-up"></span> Powyżej</a></li>
            <li><a href="#" onclick="PLC_add_row_below($('.selected'));"><span class="fa fa-hand-o-down"></span> Poniżej</a></li>
        </ul>


    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Dodaj kolumnę
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#" onclick="PLC_add_column_left($('.selected'));"><span class="fa fa-hand-o-left"></span> Z lewej</a></li>
            <li><a href="#" onclick="PLC_add_column_right($('.selected'));"><span class="fa fa-hand-o-right"></span> Z prawej</a></li>
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#PLC_etykiety">Etykiety</button>
    </div>

    <div class="btn-group" role="group">
        <?php foreach ($elements->SVG as $element => $SVG) { ?>
            <button type="button" class="btn btn-default" onclick="PLC_add_element('<?= $element ?>')"><?= $SVG ?></button>
        <?php } ?>
    </div>

</div>