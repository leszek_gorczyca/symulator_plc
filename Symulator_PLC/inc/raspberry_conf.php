<script>
    $(function () {
        $('#PLC_raspberry').on('hide.bs.tooltip', function () {
            PLC_raspberry.set_config();
        });
    });
</script>
<div class="modal" tabindex="-1" role="dialog" id="PLC_raspberry">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ustawienia Raspberry</h4>
            </div>
            <div class="modal-body">
                <figure class="image">
                    <img src="images/gpio-numbers-pi2.png" style="max-width: 100%"/>
                    <figcaption style="text-align: center;">Oznaczenie pinów GPIO raspatory PI. Źródło: <a href="https://www.raspberrypi.org/documentation/usage/gpio-plus-and-raspi2/README.md">www.raspberrypi.org</a></figcaption>
                </figure>
                <h3>Konfiguracja GPIO:</h3>
                <form class="form-horizontal">
                    <?php for ($i = 0; $i <= 27; $i++) { ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="PLC_gpio_<?= $i ?>">GPIO <?= $i ?></label>
                            <div class="col-sm-2">
                                <select  id="PLC_gpio_<?= $i ?>" class="form-control PLC_gpio">
                                    <option value="">-</option>
                                    <option value="in">Wejście</option>
                                    <option value="out">Wyjście</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label" for="PLC_gpio_<?= $i ?>_adres">Adres: </label>
                            <div class="col-sm-2">
                                <input type="text" disabled="disabled" id="PLC_gpio_<?= $i ?>_adres" class="form-control PLC_gpio_adres" value="">
                            </div>

                        </div>
                    <?php } ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary">Zamknij</button>
            </div>
        </div>
    </div>
</div>