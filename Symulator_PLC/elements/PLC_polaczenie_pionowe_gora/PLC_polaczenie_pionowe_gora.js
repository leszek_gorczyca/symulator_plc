var PLC_polaczenie_pionowe_gora = {
    name: "Połączenie pionowe",
    description: "",
    operands: [],
    inserted: function () {
        // określa kierunkiw który powinna biegnąć linia
        var l = false;
        var g = false;
        var p = false;
        var d = false;

        //odniesienie do obiektu
        var myself = PLC_get_selected();

        // domyślna klasa góra i dół
        var e_class = '';

        // określa sąsiadów elementu
        var sasiad_l = PLC_get_element_neighbour(0);
        var sasiad_g = PLC_get_element_neighbour(1);
        var sasiad_p = PLC_get_element_neighbour(2);
        var sasiad_d = PLC_get_element_neighbour(3);


        e_class += (sasiad_l && !sasiad_l.hasClass('PLC_empty') ? "L" : "x");
        e_class += "G";
        e_class += (sasiad_p && !sasiad_p.hasClass('PLC_empty') ? "P" : "x");
        e_class += "x";



        myself.addClass(e_class);

    },
    evaluate: function() {
        if (PLC_sim.get_class_left() == 'PLC_empty') { // po lewej stronie nie znajduje się żaden element
            var value_top = PLC_sim.get_input_top();
            if (value_top === null) { // jeśli element powyżej nie został jescze obliczony zwracamy stan niski
                return false;
            } else {
                return value_top;
            }
        } else { // po lewej stronie znajduje sie element
            return PLC_sim.get_input_left();
        }
    }
};