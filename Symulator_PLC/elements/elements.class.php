<?php

class PLC_Elements {

    public $directory;
    public $elements;
    public $SVG;
    public $SVG_link;

    function __construct($directory = __DIR__) {


        /**
         * Sprawdza czy zmienna zawiera kropke 
         *  
         * @param type $e
         * @return type
         */
        function PLC_hasdot($e) {
            return (strpos($e, '.') === FALSE);
        }

        $this->directory = $directory;

        $elements = scandir($this->directory);
        $this->elements = array_values(array_filter($elements, "PLC_hasdot"));
        rsort($this->elements);
        $this->load_svg();
        $this->load_svg_links();
    }

    /**
     * Ładuje do przeglądarki kod javascript elementów
     */
    function js() {
        header("Content-type: application/x-javascript");
        header("X-Content-Type-Options: nosniff");
        echo 'var PLC_elements = ' . json_encode($this->elements) . ';';

        foreach ($this->elements as $element) {
            if (file_exists(__DIR__ . '/' . $element . '/' . $element . '.js')) {
                echo file_get_contents(__DIR__ . '/' . $element . '/' . $element . '.js') . PHP_EOL;
            }
        }
    }

    /**
     *  Ładuje do przeglądarki style poszczególnych elementów
     * TODO: dodać datę ostatniej zmiany aby zapobiec przeładowaniu CSS przez przeglądarkę
     */
    function css() {
        header("Content-type: text/css");
        header("X-Content-Type-Options: nosniff"); // dla internet explorera
        foreach ($this->elements as $element) {
            if (file_exists(__DIR__ . '/' . $element . '/' . $element . '.css')) {
                echo file_get_contents(__DIR__ . '/' . $element . '/' . $element . '.css') . PHP_EOL;
            }
        }
    }

    /**
     * Wczytuje listę elementów i ich kod SVG symboli
     */
    function load_svg() {
        foreach ($this->elements as $element) {
            if (file_exists(__DIR__ . '/' . $element . '/' . $element . '.svg')) {
                $this->SVG[$element] = file_get_contents(__DIR__ . '/' . $element . '/' . $element . '.svg');
            }
        }
    }

    /**
     * Wczytuje listę elementów i linki do ich symbolów SVG
     */
    function load_svg_links() {
        foreach ($this->elements as $element) {
            $this->SVG_link[$element] = 'elements/' . $element . '/' . $element . '.svg';
        }
    }

}

$elements = new PLC_Elements();
