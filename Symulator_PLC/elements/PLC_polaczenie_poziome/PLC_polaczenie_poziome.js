var PLC_polaczenie_poziome = {
    name: "Połączenie poziome",
    description: "",
    operands: [],
    inserted: function () {
        return;
    },
    /**
     * Połączenie poziome, przekazuje sygnał dalej
     * 
     * @param {type} input Boolean
     * @returns {Boolean}
     */
    evaluate: function() {
        return PLC_sim.get_input_left();
    }
};