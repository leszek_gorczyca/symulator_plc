var PLC_polaczenie_pionowe_dol = {
    name: "Połączenie pionowe",
    description: "",
    operands: [],
    inserted: function () {
        // określa kierunkiw który powinna biegnąć linia
        var l = false;
        var g = false;
        var p = false;
        var d = false;

        //odniesienie do obiektu
        var myself = PLC_get_selected();

        // domyślna klasa góra i dół
        var e_class = '';

        // określa sąsiadów elementu
        var sasiad_l = PLC_get_element_neighbour(0);
        var sasiad_g = PLC_get_element_neighbour(1);
        var sasiad_p = PLC_get_element_neighbour(2);
        var sasiad_d = PLC_get_element_neighbour(3);


        e_class += (sasiad_l && !sasiad_l.hasClass('PLC_empty') ? "L" : "x");
        e_class += "x";
        e_class += (sasiad_p && !sasiad_p.hasClass('PLC_empty') ? "P" : "x");
        e_class += "D";

        myself.addClass(e_class);

    },
    evaluate: function () {
        // element po lewej stronie jest pusty <- początek
        if (PLC_sim.get_class_left() === 'PLC_empty') {
            var el_bottom = PLC_sim.get_input_bottom();
            if (el_bottom === null) { //element poniżej nie został jescze obliczony
                PLC_sim.jump_to_row(PLC_sim.current_row + 1, PLC_sim.current_column);
            } else { // element poniżej obliczony zwracamy jego wartość
                return el_bottom;
            }
        } else { //mamy element po lewej stronie
            var el_bottom = PLC_sim.get_input_bottom();
            var value_left = PLC_sim.get_input_left();
            if (el_bottom === null) { //element poniżej nie został jescze obliczony
                if (value_left === true) { //element po lewej ma stan wysoki
                    PLC_sim.set_bottom_value(value_left); //ustawiamy stan wysoki poniżej
                    return true;
                } else { //element po lewej ma stan niski, zanim kontynuujemy musimy obliczyc element ponizej
                    PLC_sim.jump_to_row(PLC_sim.current_row + 1, PLC_sim.current_column);
                }
            } else { //element ponizej jest juz obliczny
                return value_left || el_bottom; // zwracamy wynik alternatywy stanu elementu po lewej i ponizej
            }
        }



    }
};