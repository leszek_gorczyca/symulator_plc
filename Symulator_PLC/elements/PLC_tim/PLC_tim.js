// lista aktywnych timerów po dodaniu timera do tej listy jego wartosc bedzie zmniejszania co 0.1s
var PLC_timers_active = [];

var PLC_tim = {
    name: "Timer",
    description: "Timer zliczający w dół. (uwaga: element kończący, nie mogą się po nim znajdować żadne inne elementy)",
    operands: ["Adres", "Wartość nastawiona"],
    validator: [/^%MW1\.[0-3]$/gi, /^#\d{1,4}$/gi],
    inserted: function () {
        return;
    },
    evaluate: function () {
        //TO DO dopisac sprawdzanie zbocza
        var raising = PLC_sim.get_input_left_raising(); // sprawdzamy czy wejsce jest rosnace
        var enabled = PLC_sim.get_input_left(); // sprawdzamy czy stan wejscia jest wysoki
        var timer = PLC_sim.get_current_operands()[0];
        var index = -1;
        var set_vale = dec2bcd(PLC_sim.get_current_operands()[1].substr(1)); // poprawie wartosci set value i konwersja na kod BCD
        if ((index = PLC_timers_active.indexOf(timer)) !== -1) { //timer jest na liście aktywnych timerów
            if (!enabled) { //stan wejscia timer jest niski
                PLC_timers_active.splice(index, 1); //usunięcie timera z listy aktywnych timerów
                PLC_mem.wrtieWord(timer, set_vale); //wpisanie set value do adresu timera
            }
        } else { //timera nie ma na liście aktywnych timerów
            if (raising) { //stan wejscia timer jest niski
                PLC_mem.wrtieWord(timer, set_vale); //wpisanie set value do adresu timera
                PLC_timers_active.push(timer); //dodanie do listy aktywnych timerów
            } else if (!enabled) { //timer nie jest aktywny i nie ma go na liśnie aktywnych timerów
                PLC_mem.wrtieWord(timer, set_vale); //wpisanie set value do adresu timera
            }
        }
        return enabled;
    },
    /**
     * Funkcja wywoływana w momencie inicjalizacji elementu
     * 
     * @returns {undefined}
     */
    init: function() {
        var timer = PLC_sim.get_current_operands()[0]; //adres timera
        var set_vale = dec2bcd(PLC_sim.get_current_operands()[1].substr(1)); //wartość set value zamieniona na kod BCD
        PLC_mem.wrtieWord(timer, set_vale); //ustawienie początkowej wartosci w pamieci
    }

};

/**
 * Funkcja wywoływana co 0.1 sekundy
 * 
 * @returns {undefined}
 */
function timerTick() {
    var i = 0;
    while (i < PLC_timers_active.length) {
        var timer = PLC_timers_active[i];
        var wartosc = bcd2dec(PLC_mem.readWord(timer)); //aktualna wartosc timera
        if (wartosc > 0) { //nie osiagnieto zera
            PLC_mem.wrtieWord(timer, dec2bcd(wartosc - 1)); //zmniejsz wartosc timera
            i++;
        } else { //usun timer z lity aktywnych timerow
            PLC_timers_active.splice(i, 1);
        }
    }
}

PLC_sim.callback_0_1s.push('timerTick'); // dodanie funkcji timera do listy f-kcji wywołuwanych co 0.1s

