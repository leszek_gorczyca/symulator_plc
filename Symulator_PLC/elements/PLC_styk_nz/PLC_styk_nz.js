var PLC_styk_nz = {
    name: "Styk normalnie zamknięty",
    description: "Testuje stan skojarzonej z nim zmiennej. Jeśli zmienna ma wartość logiczną FALSE (0) stan z lewej strony przenoszony jest na prawą stronę.",
    operands: ["Adres"],
    validator: [/^%[IM][XW][0-3]\.([0-9]|1[0-5])$/gi],
    inserted: function () {
        return;
    },
    evaluate: function () {
        return PLC_sim.get_input_left() && !PLC_mem.read(PLC_sim.get_current_operands()[0]); //zwaraca zanegowany stan pamieci o adresie okreslonej przez pierwszy (indeks 0) operand
    }
};

    