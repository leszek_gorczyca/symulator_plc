
/**
 * Dla zadanej tablicy kirunków zwraca klase je określającą.
 * Kolejnośc kierunków w tablicy jest bez znaczeia
 *   
 * @param {Array} a Lista kierunków - tablica, mozliwe elmenety: [L, G, P, D] - lewo, góra, prawo, dół
 * @returns {String}
 */
function PLC_get_directions_class_name(a) {
    var className = '';
    className += (a.indexOf('L') !== -1 ? "L" : "x");
    className += (a.indexOf('G') !== -1 ? "G" : "x");
    className += (a.indexOf('P') !== -1 ? "P" : "x");
    className += (a.indexOf('D') !== -1 ? "D" : "x");
    return className;
}

/**
 * Określa kierunki połączeń elementu
 * 
 * @param {type} e Element
 * @returns {Array} Tablica kierunków L, G, P, D (lewo, góra, prawo dół)
 */
function PLC_get_directions(e) {

    var directions = [];
    var possibleDirections = ['L', 'G', 'P', 'D'];
    var re = /(x|L)(x|G)(x|P)(x|D)/i;
    var classes = e.classes();
    var classesLength = classes.length;

    for (var i = 0; i < classesLength; i++) {
        var matches = classes[i].match(re);
        if (matches !== null) {
            directions = intersect(matches, possibleDirections);
            break;
        }
    }
    return directions;
}

/**
 * Uaktualnia połączenia poziomowe dla sąsiadujących z zaznaczonym elementem połączeń pionowych
 * 
 * @returns {undefined}
 */
function PLC_update_neighbours() {
    var sasiad_l = PLC_get_element_neighbour(0);
    var sasiad_p = PLC_get_element_neighbour(2);
    var directions = [];
    var class_remove = '';
    var class_add = '';

    // uaktualnienie sąsiada z lewej strony
    if (sasiad_l && (sasiad_l.hasClass("PLC_polaczenie_pionowe") || sasiad_l.hasClass("PLC_polaczenie_pionowe_gora") || sasiad_l.hasClass("PLC_polaczenie_pionowe_dol"))) {
        directions = PLC_get_directions(sasiad_l);
        if (directions.indexOf('P') === -1) { // jeśli sąsiad z lewej strony nie ma połączenia na prawo
            class_remove = PLC_get_directions_class_name(directions); // określenie klasy starych połączeń
            directions.push('P');
            class_add = PLC_get_directions_class_name(directions); // określenie klasy nowych połączeń
            sasiad_l.removeClass(class_remove); //usunięcie starych połączeń
            sasiad_l.addClass(class_add); // dodanie nowych
        }
    }
    // uaktualnienie sąsiada z prawej strony
    if (sasiad_p && (sasiad_p.hasClass("PLC_polaczenie_pionowe") || sasiad_p.hasClass("PLC_polaczenie_pionowe_gora") || sasiad_p.hasClass("PLC_polaczenie_pionowe_dol"))) {
        directions = PLC_get_directions(sasiad_p);
        if (directions.indexOf('L') === -1) { // jeśli sąsiad z prawej strony nie ma połączenia na lewo
            class_remove = PLC_get_directions_class_name(directions); // określenie klasy starych połączeń
            directions.push('L');
            class_add = PLC_get_directions_class_name(directions); // określenie klasy nowych połączeń
            sasiad_p.removeClass(class_remove); //usunięcie starych połączeń
            sasiad_p.addClass(class_add); // dodanie nowych
        }
    }
}
/**
 * 
 * @param {Object} sender pole tekstowe wywołujący validator
 * @param {String} e_class
 * @param {Number} i
 * @returns {Boolean}
 */
function PLC_operand_validation(sender, e_class, i) {
    var sender = $(sender);
    var value = sender.val();
    if (value.match(window[e_class]['validator'][i]) == null) {
        $('#has_plc_operand_'+i).removeClass('has-success').addClass('has-error');
        $('#has_plc_operand_'+i).find('.form-control-feedback').removeClass('glyphicon-ok').addClass('glyphicon-remove');
    } else {
        $('#has_plc_operand_'+i).removeClass('has-error').addClass('has-success');
         $('#has_plc_operand_'+i).find('.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok');
    }
    
}

/**
 * Zamienia liczbe w postaci dziesietnej na kod BCD
 * 
 * @param {type} dec
 * @returns {String}
 */
function dec2bcd(dec) {
    var bcd = '0000000000000000';
    dec = dec.toString().split(''); // rozbicie liczby na pojedyncze cyfry
    for (i=0; i<dec.length; i++) {
        bcd += ('0000'+((Number(dec[i])).toString(2))).substr(-4);
    }
    return bcd.substr(-16);
}

function bcd2dec(bcd) {
    var size = 4;
    var dec = '';
    for (i=0; i<(bcd.length/size); i++) {
        var digit = parseInt(bcd.substr(i*size, size), 2);
        dec += digit;
    }
    return Number(dec);
}

/**
 * Zwraca nową kopie tablicy
 * 
 * @param {Array} a
 * @returns {Array|Object}
 */
function array_new_copy(a) {
    return JSON.parse(JSON.stringify(a));
}

/**
 * Zwraca pierwszy wolny adres nie używany przez żaden z GPIO z danego obszaru
 * 
 * @param {type} area
 * @returns {undefined}
 */

function get_free_addres(area) {
    //pobranie wszytski używanych adresów
    var in_use = [];
    in_use = $('.PLC_gpio_adres').map(function() {
        return this.value;
    }).get();
    
    //szukanie pierwszego wolnego adresu
    for (var i = 0; i < PLC_mem.memory; i++) {
        for (var j = 0; j < PLC_mem.arch; j++) {
            var adres = area+i+'.'+j;            
            if (in_use.indexOf(adres) === -1) { //jeśli adres jest wolny
                return adres; // zwracamy adres
            }
        }
    }
    return ''; // zwracam pusty adres jeśli wszystkie są zajete
}

var PLC_empty = {
    name: "- pusty -",
    description: "Nie ma wpływu na wykonanie programu",
    operands: [],
    validator: [],
    inserted: function() {
        return;
    },
    evaluate: function() {
        return false;
    }
    
};