var PLC_cewka_no = {
    name: "Cewka",
    description: "Przekazuje do skojarzonej z nią zmiennej stan elementu po lewej stronie. (uwaga: element kończący, nie mogą się po nim znajdować żadne inne elementy)",
    operands: ["Adres"],
    validator: [/^%[QM]X[0-3]\.([0-9]|1[0-5])$/gi],
    inserted: function () {
        return;
    },
    evaluate: function () {
        return PLC_mem.write(PLC_sim.get_current_operands()[0], PLC_sim.get_input_left()); //Zapisuje wartosc elementu po lewej stonie w pamieci
    }

};