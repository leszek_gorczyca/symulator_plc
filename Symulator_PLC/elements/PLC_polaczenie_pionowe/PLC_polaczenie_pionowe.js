var PLC_polaczenie_pionowe = {
    name: "Połączenie pionowe",
    description: "",
    operands: [],
    inserted: function () {
        // określa kierunkiw który powinna biegnąć linia
        var l = false;
        var g = false;
        var p = false;
        var d = false;

        //odniesienie do obiektu
        var myself = PLC_get_selected();

        // domyślna klasa góra i dół
        var e_class = '';

        // określa sąsiadów elementu
        var sasiad_l = PLC_get_element_neighbour(0);
        var sasiad_g = PLC_get_element_neighbour(1);
        var sasiad_p = PLC_get_element_neighbour(2);
        var sasiad_d = PLC_get_element_neighbour(3);

        // określa kierunki w których powinny być dodane połączenia
        e_class += (sasiad_l && !sasiad_l.hasClass('PLC_empty') ? "L" : "x");
        e_class += "G";
        e_class += (sasiad_p && !sasiad_p.hasClass('PLC_empty') ? "P" : "x");
        e_class += "D";

        myself.addClass(e_class);

    },
    evaluate: function () {
        var el_bottom = PLC_sim.get_input_bottom();
        var el_top = PLC_sim.get_input_top();
        var cl_left = PLC_sim.get_class_left();
        if (cl_left === 'PLC_empty') {
            if (el_bottom === null && el_top === null) { //element poniżej i powyżej nie został jescze obliczony
                PLC_sim.jump_to_row(PLC_sim.current_row + 1, PLC_sim.current_column);
                
            } else { // element poniżej/powyżej obliczony zwracamy jego wartość
                return Boolean(el_bottom || el_top);
            }
        } else { // mamy element po lewej stronie
            
            var el_left = PLC_sim.get_input_left();
            if (el_left == false) { // jeśli element po lewej stronie ma wartosc false
                if (el_bottom == null) { //elementy poniżej nie został jeszcze obliczony
                    PLC_sim.jump_to_row(PLC_sim.current_row + 1, PLC_sim.current_column); //skaczemy do elementu poniżej aby go obliczyć
                } else {
                    return Boolean(el_bottom || el_left); //zwracamy alternatywe elementu poniżej i po lewej
                }
            }
            else {
                return el_left;
            }
             
        }
    }
};