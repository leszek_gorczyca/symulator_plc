function PLC_zapisz() {
    $('#PLC_save').find('.btn.btn-primary').prop('disabled', true);
    $.post("save.php", {xml: $('#PLC_main').html(), name: $('#PLC_zapisz_name').val(), labels: JSON.stringify([PLC_labels.labels, PLC_labels.addresses])})
            .done(function (data) {
                $('#PLC_save').modal('hide');
                $('#PLC_save').find('.btn.btn-primary').prop('disabled', false);
            }
            );
}

function PLC_wczytaj() {
    $('#PLC_load').find('.btn.btn-primary').prop('disabled', true);
    $.get("load.php", {name: $('#PLC_wczytaj_name').val()})
            .done(function (data) {               
                var data_obj = JSON.parse(data);
                // wczytanie programu
                $('#PLC_main').html(data_obj.program);
                // wczytanie etykiet
                var etykiety = JSON.parse(data_obj.etykiety);
                PLC_labels.add_many(etykiety[1], etykiety[0]); // 0 - etykieta; 1 - adres
                for (i = 0; i < etykiety[0].length; i++) {
                    PLC_etykiety_dodaj_wiersz(etykiety[1][i], etykiety[0][i]);
                }
                //ukrycie okienka
                $('#PLC_load').modal('hide');
                $('#PLC_load').find('.btn.btn-primary').prop('disabled', false);
                
            }
            );
}

function PLC_ladder_refresh() {

}

/**
 * Zwraca wysokość drabiny - ilość szczebli
 * 
 * @returns {Number} - Ilośc szczebli
 */
function PLC_ladder_height() {
    return parseInt($('.PLC_rung').last().attr('data-rung')) + 1;
}

/**
 * Zwraca szerokość programu - ilość kolumn dodanych przez użytkownika.
 * Ilość kolumn określania jest na podstawie ostatniego indeksu kolumny.
 * 
 * @returns {Number} ilość kolumn (liczba całkowita)
 */

function PLC_ladder_width() {
    return parseInt($('.PLC_column').last().attr('data-column')) + 1;
}

/**
 * Zwraca liczbę wierszy w szczeblu.
 * Szczebel jest określany na podstawie zadanego elementu.
 * Ilośc wierszy jest określania na podstawie indeksu ostatniego wiersza w szczeblu
 * 
 * @param {Object} e element do określenia szczebla
 * @returns {Number} ilośc wierszy na szczeblu okleślionym przez element e
 */
function PLC_ladder_rung_height(e) {
    e = PLC_get_rung(e);
    return parseInt(e.find('.PLC_row').last().attr('data-row')) + 1;
}

/**
 * Zwraca element wiersza na ktorym znajduję się zadany element e
 * 
 * @param {Object} e element do określenia wiersza
 * @returns {Object} Element wiersza
 */
function PLC_get_row(e) {
    return e.closest('.PLC_row');
}

/**
 * Zwraca element szczebla na którym znajduję się zadany element e
 * 
 * @param {Object} e Element do określenia szczebla
 * @returns {Object} Elemenr szczebla
 */
function PLC_get_rung(e) {
    return e.closest('.PLC_rung');
}

/**
 * Wyświetla okienko dodawania elementu
 * 
 */
function PLC_add_element_modal() {
    $('#PLC_add_element_modal').modal();
}

/**
 * usuwa aktualnie zaznaczony element
 * 
 */
function PLC_delete_selected() {
    PLC_get_selected().removeClass().addClass('PLC_column').addClass('selected');
}

/**
 * Przygotowuje okienko do edycji elementu
 * 
 * @param {String} e_class Nazwa elementu - klasa
 */
function PLC_show_element_modal(e_class) {
    var operands = window[e_class]['operands'].length; // sprawdzenie ilości operandów
    if (operands === 0) { // jeśli element nie ma żadnych operandów zwracamy zero
        return;
    }
    $('#PLC_element_modal').find('form').empty(); // wyczyszczenie formularza
    for (var i = 0; i < operands; i++) { // dodawanie w petli operandow
        var operand_name = 'plc_operand_' + i;
        // sprawdzamy czy element ma już ustawione jakieś operandy
        var value = PLC_get_selected().attr('data-' + operand_name);
        value = (typeof value === 'undefined') ? '' : value; // jesli nie to wartośc jest pustym polem
        $('#PLC_element_modal').find('form').append('<div class="form-group has-feedback" id="has_' + operand_name + '"><label for="' + operand_name + '" class="control-label">' + window[e_class]['operands'][i] + '</label><input onkeyup="PLC_operand_validation(this, \'' + e_class + '\', ' + i + ')" type="text" class="form-control" value ="' + value + '" id="' + operand_name + '"><span class="glyphicon form-control-feedback"></span></div>');
    }

    $('#PLC_element_modal').find('.modal-title').text(window[e_class]['name']); // ustawienie tytulu okienka
    $('#PLC_element_modal').modal(); // wywołuje okienko z ustawieniami
}

/**
 * Wstawia element o nazwie e_class w miejscu aktualnie zaznaczonym
 * 
 * @param {String} e_class Nazwa elementu do wstawienia
 * @returns {undefined}
 */
function PLC_add_element(e_class) {
    PLC_delete_selected(); // usuwa element jeśli istnieje
    e = PLC_get_selected().addClass(e_class);
    window[e_class]['inserted'](); // wywołuje metodę inserted dla nowo wstawionej klasy
    PLC_update_neighbours(); // uaktualnia sąsiadów
    PLC_element_over(e); // uaktualnia informacje (symulacje najechania mysza)
    PLC_show_element_modal(e_class); // wyświetla okienko z ustawieniami

}
/**
 * Zapisuje operandy związane z aktualnie zaznaczonym elementem element
 * @returns {undefined}
 */
function PLC_update_element() {
    var element = PLC_get_selected();
    var e_class = PLC_get_element_class(element);
    var operands = window[e_class]['operands'].length; // sprawdzenie ilości operandów
    for (var i = 0; i < operands; i++) {
        element.attr('data-plc_operand_' + i, $('#plc_operand_' + i).val());
    }
    $('#PLC_element_modal').modal('hide');
    PLC_add_description(element);

}
/**
 * Zwraca aktualnie zaznaczony element
 * 
 * @returns {Object}
 */
function PLC_get_selected() {
    return $('.selected');
}


/**
 * Odświeżenie szerokości tabeli po dodaniu kolumny
 * 
 * @returns
 */
function PLC_add_column_resize() {
    $('.PLC_rung').width($('.PLC_rung').width());
}


/**
 * Dodaje kolumnę do programu po lewej stronie od elementu e.
 * Indeksy pozostałych kolumn zostają przeliczone.
 * 
 * @param {Object} e element po którego lewej stronie zostanie wstawiona kolumna
 * @returns {undefined}
 */
function PLC_add_column_left(e) {

    var column = parseInt(e.attr('data-column'));
    var column_count = PLC_ladder_width();

    //uaktualnienie indeksów kolumn
    for (var i = (column_count - 1); i >= column; i--) {
        $('.PLC_column[data-column="' + i + '"]').attr('data-column', (i + 1));
    }

    $('.PLC_column[data-column="' + (column + 1) + '"]').before('<td class="PLC_column PLC_empty" data-column="' + column + '">');
    PLC_add_column_resize();
    PLC_ladder_refresh();
}

/**
 * Dodaje kolumnę do programu po prawej stronie od elementu e.
 * Indeksy pozostałych kolumn zostają przeliczone.
 * 
 * @param {Object} e element po którego prawej stronie zostanie wstawiona kolumna
 * @returns {undefined}
 */
function PLC_add_column_right(e) {

    var column = parseInt(e.attr('data-column'));
    var column_count = PLC_ladder_width();

    //uaktualnienie indeksów kolumn
    for (var i = (column_count - 1); i > column; i--) {
        $('.PLC_column[data-column="' + i + '"]').attr('data-column', (i + 1));
    }

    $('.PLC_column[data-column="' + (column) + '"]').after('<td class="PLC_column PLC_empty" data-column="' + (column + 1) + '">');
    PLC_add_column_resize();
    PLC_ladder_refresh();
}

/**
 * Zwraca numer wiersza w którym znajduję się element e
 * 
 * @param {Object} e Element do określenia numeru wiersza
 * @returns {Number} Numer wiersza
 */
function PLC_get_row_index(e) {
    e = PLC_get_row(e);
    return parseInt(e.attr('data-row'));
}

/**
 * Zwraca numer kolumny dla zadanego elemetu
 * 
 * @param {type} e Element do określenia numeru kolumny
 * @returns {Number}
 */
function PLC_get_coulmn_index(e) {
    return parseInt(e.attr('data-column'));
}
/**
 * Zwraca element dla zadanego szczebla, wiersza i kolumny
 * Zwraca false jeśli element nie istnieje
 * 
 * @param {Number} rung Szczebel
 * @param {Number} row Wiersz
 * @param {Number} column Kolumna
 * @returns {Object}
 */
function PLC_get_element(rung, row, column) {
    var e = $('.PLC_rung[data-rung="' + (rung) + '"]').find('.PLC_row[data-row="' + (row) + '"]').find('.PLC_column[data-column="' + (column) + '"]');
    if (e.length === 0) {
        return false;
    } else {
        return e;
    }
}
/**
 * Zaznacza element 
 * 
 * @param {type} rung
 * @param {type} row
 * @param {type} column
 * @param {type} deselect
 * @returns {e}
 */
function PLC_set_element_selected(rung, row, column, deselect) {
    deselect = typeof deselect !== 'undefined' ? deselect : true;
    if (deselect) {
        PLC_get_selected().removeClass('selected');
    }
    e = PLC_get_element(rung, row, column).addClass('selected');
    PLC_element_over(e);
    return e;
}
/**
 * Reprezentacja graficznach elementu znajdjącego się w stanie wysokim
 * 
 * @param {type} rung
 * @param {type} row
 * @param {type} column
 * @returns {undefined}
 */
function PLC_set_element_high(rung, row, column) {
    PLC_get_element(rung, row, column).addClass('high');
}

/**
 * Reprezentacja graficznach elementu znajdjącego się w stanie niskim
 * 
 * @param {type} rung
 * @param {type} row
 * @param {type} column
 * @returns {undefined}
 */
function PLC_set_element_low(rung, row, column) {
    var e = PLC_get_element(rung, row, column);
    if (!e.hasClass('PLC_empty')) { //element nie jest pusty
        e.addClass('low');
    }

}

/**
 * Zwraca numer szczebla na którym znajduję się element e
 * 
 * @param {Object} e Element do określenia numeru szczebla
 * @returns {Number} 
 */
function PLC_get_rung_index(e) {
    e = PLC_get_rung(e);
    return parseInt(e.attr('data-rung'));
}
/**
 * Przygotowuje zawartość wiersza do wstawienia do szczebla.
 * Ilośc kolumn w wierszu dostosowana jest do aktualnej szerokości drabinki
 * 
 * @param {Number} wiersz pozycja wiersza do wstawienia
 * @returns {String}
 */
function PLC_render_row(wiersz) {
    var columns = PLC_ladder_width();
    var htmlColumns = '';

    for (var i = 0; i < columns; i++) {
        htmlColumns += '<td class="PLC_column PLC_empty" data-column="' + (i) + '">';
    }
    return '<tr class="PLC_row" data-row="' + wiersz + '">' + htmlColumns + '</tr>';
}

/**
 * Wstawia nowy wiersz do programu powyżej elementu e
 * Indeksy pozostałych wierszy sa przeliczane
 * 
 * @param {type} e Element powyżej którego zostanie wstawiony nowy wiersz
 * @returns {undefined}
 */
function PLC_add_row_above(e) {
    // aktualna pozycja
    var rung = PLC_get_rung_index(e);
    var row = PLC_get_row_index(e);
    var row_count = PLC_ladder_rung_height(e);
    for (var i = (row_count - 1); i >= row; i--) {
        $('.PLC_rung[data-rung="' + (rung) + '"]').find('.PLC_row[data-row="' + (i) + '"]').attr('data-row', (i + 1));
    }
    $('.PLC_rung[data-rung="' + (rung) + '"]').find('.PLC_row[data-row="' + (row + 1) + '"]').before(PLC_render_row(row));
    PLC_ladder_refresh();
}

/**
 * Wstawia nowy szczebel powyżej sczebla w którcym znajduje się przekazany element e
 * @param {type} e element powyżej którego zostanie wstawiony szczebel
 */
function PLC_add_rung_above(e) {
    var e = PLC_get_selected();
    // aktualna pozycja
    var rung = PLC_get_rung_index(e);
    var rung_count = PLC_ladder_height();
    // przeliczenie indeksów istniejących szczebli
    for (var i = (rung_count - 1); i >= rung; i--) {
        $('.PLC_rung[data-rung="' + (i) + '"]').find('.PLC_rung_id').text(i + 1); // uaktualnienie opisów szczebli
        $('.PLC_rung[data-rung="' + (i) + '"]').attr('data-rung', (i + 1));
    }
    $('.PLC_rung[data-rung="' + (rung + 1) + '"]').before('<table class="PLC_rung" data-rung="' + (rung) + '"><caption><small>Szczebel:&nbsp;<span class="PLC_rung_id">' + (rung) + '</span></small></caption>' + PLC_render_row(0) + '</table>');
    PLC_ladder_refresh();
}

function PLC_add_rung_below() {
    var e = PLC_get_selected();
    // aktualna pozycja
    var rung = PLC_get_rung_index(e);
    var rung_count = PLC_ladder_height();
    // przeliczenie indeksów istniejących szczebli
    for (var i = (rung_count - 1); i > rung; i--) {
        $('.PLC_rung[data-rung="' + (i) + '"]').find('.PLC_rung_id').text(i + 1); // uaktualnienie opisów szczebli
        $('.PLC_rung[data-rung="' + (i) + '"]').attr('data-rung', (i + 1));
    }
    $('.PLC_rung[data-rung="' + (rung) + '"]').after('<table class="PLC_rung" data-rung="' + (rung + 1) + '"><caption><small>Szczebel:&nbsp;<span class="PLC_rung_id">' + (rung + 1) + '</span></small></caption>' + PLC_render_row(0) + '</table>');
    PLC_ladder_refresh();
}

/**
 * Wstawia nowy wiersz do programu poniżej elementu e
 * Indeksy pozostałych wierszy sa przeliczane
 * 
 * @param {type} e Element poniżej którego zostanie wstawiony nowy wiersz
 * @returns {undefined}
 */
function PLC_add_row_below(e) {
    // aktualna pozycja
    var rung = PLC_get_rung_index(e);
    var row = PLC_get_row_index(e);
    var row_count = PLC_ladder_rung_height(e);
    for (var i = (row_count - 1); i > row; i--) {
        $('.PLC_rung[data-rung="' + (rung) + '"]').find('.PLC_row[data-row="' + (i) + '"]').attr('data-row', (i + 1));
    }
    $('.PLC_rung[data-rung="' + (rung) + '"]').find('.PLC_row[data-row="' + (row) + '"]').after(PLC_render_row(row + 1));
    PLC_ladder_refresh();
}
/**
 * Zwraca "sąsiada" aktualnie zaznaczonego elementtu
 * Wymaga podania kierunku
 * 0 - lewo
 * 1 - góra
 * 2 - prawo
 * 3 - dół
 * 
 * @param {Integer} direction kod kieruneku 0-3 (lewo, góra, prawo, dół)
 * @returns {Object} Element, lub false w przypadku kiedy nie ma sąsiada
 */
function PLC_get_element_neighbour(direction) {
    var element = PLC_get_selected();
    var rung = PLC_get_rung_index(element);
    var row = PLC_get_row_index(element);
    var column = PLC_get_coulmn_index(element);
    switch (direction) {
        case 0: // lewo
            return PLC_get_element(rung, row, column - 1);
            break;
        case 1: // góra
            return PLC_get_element(rung, row - 1, column);
            break;
        case 2: // prawo
            return PLC_get_element(rung, row, column + 1);
            break;
        case 3: // dół
            return PLC_get_element(rung, row + 1, column);
            break;
    }
}
/**
 * Zwraca podstawową klasę (nazwę) elementu e
 * 
 * @param {type} e Element do określenia klasy
 * @returns {Array} Klasa elementu
 */
function PLC_get_element_class(e) {
    return intersect(e.classes(), PLC_elements);
}

/**
 * Akcja wywoływana w moment podświetlenia elementu
 * 
 * @param {type} e Zaznaczony lub podświetlony element
 */
function PLC_element_over(e) {
    var rung = PLC_get_rung_index(e);
    var row = PLC_get_row_index(e);
    var column = PLC_get_coulmn_index(e);
    $('#PLC_element_nazwa').text(PLC_get_element_class(e));
    $('#PLC_pozycja_rung').text(rung);
    $('#PLC_pozycja_row').text(row);
    $('#PLC_pozycja_column').text(column);
    if (simStarted) { // stan zaznaczonego elementu wyświetlamy tylko po uruchomieniu symulacji
        $('#PLC_element_stan').text(PLC_sim.state[rung][row][column]);
    }
}

/**
 * Dodaje etykietkę do elementu
 * 
 * @param {type} e
 * @returns {undefined}
 */
function PLC_add_description(e) {
    var attr = e.attr('data-plc_operand_0');
    if (typeof attr !== 'undefined') { //sprawdzamy czy jest dodany operand
        e.html('<div class="PLC_label">' + PLC_labels.get_label(attr) + '</div>');
    }
}

var PLC_labels = {
    /**
     * Lista etykiet. Indeks etykiety odpowiada indeksowi adresu w tabeli adresów
     */
    labels: [],
    /**
     * Lista adresów. Indeks adresu odpowiada indesksowi etykiety w tabeli etykiet
     */
    addresses: [],
    
    /**
     * Dodaje etykietke dla podanego adresu
     * 
     * @param {String} address Adres
     * @param {String} label Etykieta
     * @returns {undefined}
     */
    add: function(address, label) {        
        this.addresses.push(address);
        this.labels.push(label);
    },
    
    /**
     * Dodaje wiele etykietek na raz
     * 
     * @param {type} addresses Lista dresów do dodania
     * @param {type} labels Lista etykiet do dodania
     * @returns {undefined}
     */
    add_many: function(addresses, labels) {
        this.addresses = addresses;
        this.labels = labels;
    },
    /**
     * Zwraca adres o danej etykiecie
     * 
     * @param {String} label Etykieta
     * @returns {String}
     */
    get_adress: function(label) {
        return this.addresses[this.labels.indexOf(label)];
    },
    
    /**
     * Zwraca etykietke dla danego adresu. Jeśli nie ma etykiety zwracany jest adres.
     * 
     * @param {type} address
     * @returns {Array}
     */
    get_label: function(address) {
        var index = this.addresses.indexOf(address);
        if (index === -1) {// brak etykiety dla adresu o podanej nazwie 
            return address; // zwracamy adres
        } else { //etykieta dla adresu istenieje
            return this.labels[index]; //zwraca etykietke dla danego adresu
        }
    },
    
    /**
     * Sprawdza czy dana etykietka istnieje
     * 
     * @param {type} label
     * @returns {Boolean}
     */
    exists: function(label) {
        return (this.labels.indexOf(label) !== -1);
    },
    /**
     * Sprawdza czy dany adres ma etykietkę
     * 
     * @param {type} address
     * @returns {Boolean}
     */
    has_label: function(address) {
        return (this.addresses.indexOf(address) !== -1);
    },
    /**
     * Usuwa wszystkie etykiety
     * 
     * @returns {undefined}
     */
    clear: function() {
      this.labels = [];
      this.addresses = [];
    }
};

/**
 * Określa czy obsługiwać skróty klawiaturowe
 * @type Boolean
 */
var listenKeys = true;
/**
 * Główna cześć programu uruchomiona po załadowaniu się strony
 */
$(function () {
    //wyświetla okienko powitalne
    $('#PLC_autor').modal('show');
    
    // ukrywa podglad I/O
    $('#PLC_io').hide();

    /**
     * Obsługa myszki - kliknięcie - zaznaczanie elementów
     */

    $("#PLC_main").on("click", ".PLC_column", function () {
        PLC_get_selected().removeClass('selected');
        $(this).addClass('selected');
    });
    /**
     * Obsługa myszki - podwójne kliknięcie - okienko edycji elmentu
     */
    $("#PLC_main").on("dblclick", ".PLC_column", function () {
        PLC_show_element_modal(PLC_get_element_class(PLC_get_selected()));
    });


    /**
     * Obsługa myszki - wyświetlanie informacji po najechaniu na elemnt
     */
    $("#PLC_main").on("mouseover", ".PLC_column", function () {
        PLC_element_over($(this));
    });
    $("#PLC_main").on("mouseleave", ".PLC_rung", function () {
        PLC_element_over(PLC_get_selected());
    });

    // zapobiega przesyłaniu form i przeładowaniu strony
    $("form").submit(function (event) {
        event.preventDefault();
    });



    // obsługa przeliczania checkboxów an kod BCD
    // obsluga przepisywania wartosci checkboxow z monitora pamieci do pamieci sterownika
    $(".mem :checkbox").change(function () {
        var bcd = '';
        var i = this.id.match(/MX(\d+)_/i)[1];
        for (var j = 0; j < PLC_mem.arch; j++) {
            var val = $('#MX' + i + '_' + j + ':checked').val(); //wartosc checkoboxa
            if (typeof val === 'undefined') { //checkbox nie jest zaznaczony
                bcd += '0';
                PLC_mem.M[i][j] = false; //natychmiastowe przepsianie do pamieci               
            } else { //checkbox jest zaznaczony
                bcd += '1';
                PLC_mem.M[i][j] = true; //natychmiastowe przepsianie do pamieci
            }
        }
        $("#DEC_M_" + i).val(bcd2dec(bcd)); // ustawia wartosc dziesietna
    });




    /**
     * Obsługa skrutów klawiaturowych
     */
    $(document).keydown(function (e) {
        if (!listenKeys) { // obsługa skrutów wyłączona
            return;
        }

        var preventDefault = false;
        var element = PLC_get_selected();
        var rung = PLC_get_rung_index(element);
        var row = PLC_get_row_index(element);
        var column = PLC_get_coulmn_index(element);
        switch (e.which) {

            case 33: // page up
                var element_above = PLC_get_element(rung - 1, 0, 0); // pierwszy element na szczeblu powyżej (powinien istniec - jesli nie isteniej nie ma szczebla
                if (!element_above) { // jeśli nie ma sąsiada dodajemy szczebel na górze
                    PLC_add_rung_above(element);
                    PLC_set_element_selected(rung, 0, column); // zaznaczamy nową wiersz zaznaczenie
                } else { // jeśli jest sąsiad przesuwamy zaznaczenie
                    PLC_set_element_selected(rung - 1, PLC_ladder_rung_height(element_above) - 1, column); // uzywamy sąsiada do określenia wysokości szczebla powyżej
                }
                preventDefault = true;
                break;
            case 34: //page down
                if (!PLC_get_element(rung + 1, 0, 0)) { // jeśli nie ma to wstawiamy nowy szczebel
                    PLC_add_rung_below(element);
                }
                PLC_set_element_selected(rung + 1, 0, column);

                preventDefault = true;
                break;
            case 37: // lewo
                if (!PLC_get_element_neighbour(0)) { // jeśli nie ma sąsiada dodajemy kolumnę po lewej
                    PLC_add_column_left(element);
                    PLC_set_element_selected(rung, row, column); // zaznaczamy nową kolumnę zaznaczenie
                } else { // jeśli jest sąsiad
                    PLC_set_element_selected(rung, row, column - 1); // przesuwamy zaznaczenie
                }
                preventDefault = true;
                break;

            case 38: // góra
                if (!PLC_get_element_neighbour(1)) { // jeśli nie ma sąsiada dodajemy wiersz na górze
                    PLC_add_row_above(element);
                    PLC_set_element_selected(rung, row, column); // zaznaczamy nową wiersz zaznaczenie
                } else { // jeśli jest sąsiad przesuwamy zaznaczenie
                    PLC_set_element_selected(rung, row - 1, column);
                }

                preventDefault = true;
                break;

            case 39: // prawo
                if (!PLC_get_element_neighbour(2)) {
                    PLC_add_column_right(element);
                }
                PLC_set_element_selected(rung, row, column + 1);
                preventDefault = true;
                break;

            case 40: // dół
                if (!PLC_get_element_neighbour(3)) {
                    PLC_add_row_below(element);
                }
                PLC_set_element_selected(rung, row + 1, column);
                preventDefault = true;
                break;

            default:
                return;
        }
        if (preventDefault)
            e.preventDefault(); // blokada domyślnego zachowania klawiszy
    });
    // obsługa ukrywania i pokazywania okienek
    $('.modal').on('show.bs.modal', function (e) {
        listenKeys = false;
    });
    $('.modal').on('hide.bs.modal', function (e) {
        listenKeys = true;
    });

    // włącza i wyłącza obsługę klawiszy kiedy zaznaczenie znajduje się w polu tekstowym
    $("input").focusin(function () {
        listenKeys = false;
    });
    $("input").focusout(function () {
        listenKeys = true;
    });


    // wczytywanie listy plików    
    $('#PLC_load').on('show.bs.modal', function (e) {
        $('#PLC_load').find('.btn.btn-primary').prop('disabled', true); // tymczasowe wyłączenie przycisku
        $('#PLC_wczytaj_name').empty(); // wyczyszczenie listy
        $('#PLC_wczytaj_name').append('<option value="">Wczytywanie...</option>');
        $.getJSON("load.php") // zapytanie do serwera
                .done(function (data) {
                    $('#PLC_wczytaj_name').empty();
                    for (index = 0; index < data.length; ++index) { // dodwanie elementów (nazw plików) w pętli
                        $('#PLC_wczytaj_name').append('<option value="' + data[index] + '">' + data[index] + '</option>');
                    }
                    $('#PLC_load').find('.btn.btn-primary').prop('disabled', false); // włączenie pliku
                }
                );
    });

    //Obsługa okienka konfiguracji raspberry PI

    $('.PLC_gpio').change(function () {
        var gpio = $(this);
        var gpio_adres = $('#' + gpio.attr('id') + '_adres'); //pole adresu korespondujace z wybrany GPIO
        var disabled = true;
        var value = '';
        switch (gpio.val()) {
            case "in": //jeśli usawiono jako wejście
                value = get_free_addres('%IX');
                disabled = false;

                break;
            case "out": //jeśli usawiono jako wyjście
                value = get_free_addres('%QX');
                disabled = false;
                break;
            default: //usawienie domyślne
                disabled = true;
        }

        gpio_adres.prop('disabled', disabled);
        gpio_adres.val(value);


    });

    //lista elementów do podpowiadania adresów
    PLC_mem_autocomplete = [];
    for (i = 0; i < PLC_mem.memory; i++) {
        for (j = 0; j < PLC_mem.arch; j++) {
            PLC_mem_autocomplete.push('%IX' + i + '.' + j);
            PLC_mem_autocomplete.push('%QX' + i + '.' + j);
            PLC_mem_autocomplete.push('%MX' + i + '.' + j);
        }
    }
});