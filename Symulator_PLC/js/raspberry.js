var PLC_raspberry = {
    /**
     * Tablica portów w użyciu
     * 
     * @type Array
     */
    gpio: [16, 17, 18, 19, 22, 23, 24, 25, 26, 27],
    /**
     * Konfiguracja portów (wejsce/wyjscie)
     * 
     * @type Array
     */
    gpio_direction: ["in", "out", "in", "out", "out", "in", "in", "in", "out", "out"],
    /**
     * Adres skojarzony z portem
     * 
     * @type Array
     */
    gpio_checkoboxes: ["IX0_4", "QX0_4", "IX0_0", "QX0_1", "QX0_2", "IX0_1", "IX0_2", "IX0_3", "QX0_0", "QX0_3"],
    ws: 0,
    startTime: 0,
    connect: function () {
        try {
            this.ws = new WebSocket('ws://' + $('#PLC-host').val() + ':8080');
        } catch (e) {
            alert('Błąd połączenia!');
        }
        this.ws.onopen = function (event) {
            var config = {
                type: "config",
                gpio: PLC_raspberry.gpio,
                gpio_direction: PLC_raspberry.gpio_direction
            };
            PLC_raspberry.ws.send(JSON.stringify(config));
        };
        this.ws.onmessage = function (event) {
            var message = JSON.parse(event.data);
            $('#' + PLC_raspberry.gpio_checkoboxes[message.i]).prop('checked', message.value);
        };
        this.ws.onerror = function (event) {
            alert('Błąd połączenia!');
        };
    },
    set_config: function () {
        this.gpio = [];
        this.gpio_direction = [];
        this.gpio_checkoboxes = [];
        //sprawdzanie ustawien
        for (var i = 0; i <= 27; i++) {
            var direction = $('#PLC_gpio_' + i).val();
            if (direction !== '') { // jeśli usawiono kierunek
                this.gpio.push(i);
                this.gpio_direction.push(direction);
                var adres = $('#PLC_gpio_' + i + '_adres').val();
                var matches = adres.match(/^%(I|Q|)X(\d+)\.(\d+)$/i);
                var checkbox_name = matches[1].toUpperCase() + 'X' + matches[2] + '_' + matches[3]; // określenie nazyw checkoboxa
                this.gpio_checkoboxes.push(checkbox_name);
            }
        }
    },
    write_output: function (checkbox, value) {
        // jeśli nie ejsteśmy połączeni
        if ((this.ws === 0) || (this.ws.readyState !== this.ws.OPEN)) {
            return;
        }
        // sprawdzenie czy wartość dotyczy pola które jest powiązane z raspberry
        var index = this.gpio_checkoboxes.indexOf(checkbox);
        if (index === -1) {
            return;
        }

        var message = {
            type: "value",
            gpio: index,
            gpio_value: value
        };
        this.ws.send(JSON.stringify(message));
    }
};


$(function () {
    // obsługa zmian wyjść
    $(".PLC_outputs :checkbox").change(function () {
        PLC_raspberry.write_output(this.id, this.checked);
    });
});