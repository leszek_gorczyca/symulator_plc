// określa czy symulacja została uruchomiona
var simStarted = false;
var simLoop = false;

var PLC_mem = {
    /**
     * @type Number Architektura - liczba bitów w słowie
     */
    arch: 16,
    /**
     * @type Number Wielkośc pamięci (ilośc słów)
     */
    memory: 4,
    /**
     * @type Array Obaszar pamięci wejść
     */
    I: [],
    /**
     * @type Array Obszar pamięci wyjść
     */
    Q: [],
    /**
     * 
     * @type Array Obszar pamięci wewnętrznej
     */
    M: [],
    /**
     * Inicjalizajca pamięci - wypełnienie zerami
     * @returns {undefined}
     */
    init: function () {
        for (var i = 0; i < this.memory; i++) {
            this.I.push([]);
            this.Q.push([]);
            this.M.push([]);
            for (var j = 0; j < this.arch; j++) {
                this.I[i].push(false);
                this.Q[i].push(false);
                this.M[i].push(false);
            }
        }
        this.readInput();
    },
    /**
     * Zwraca zawartośc pamięci pod podanym adresem bitu lub stan czasomierza
     * @param {type} adres
     * @returns {Number}
     */
    read: function (adres) {
        var matches = adres.match(/^%(I|Q|M)(X|W)(\d+)\.(\d+)$/i);
        var area = matches[1].toUpperCase(); // obszar
        var typ = matches[2].toUpperCase(); // typ X - bt, W - słowo
        var word = matches[3]; // numer słowa
        var bit = matches[4]; // numer bitu
        if ((area == 'M') && (typ == 'W') && (word == 1)) { // sprawdzany jest czasomierz
            var word = this.readWord(adres); // aktualna wartość timera
            return (word.indexOf('1') === -1) // jeśli timer doliczył do końca zwraca true / testujemy czy w kodzie BCD jest chociaż jedna 1 jeśli trak to wartość jest różna od zera
        }
        return this[area][word][bit];
    },
    /**
     * Zapisuje wartosc do pamieci po podanym adresem bitu
     * Dodatkowo dla bitów z zakresu M powoduje zmians stanu w monitorze pamieci
     * 
     * @param {type} adres
     * @param {type} wartosc
     * @returns {unresolved}
     */
    write: function (adres, wartosc) {
        var matches = adres.match(/^%(I|Q|M)X(\d+)\.(\d+)$/i);
        var area = matches[1].toUpperCase(); // obszar
        var word = matches[2]; // numer słowa
        var bit = matches[3]; // numer bitu
        if (area == 'M') { // ustawienie wartości checkboxa dla bitów z obszaru M
            $('#MX' + word + '_' + bit).prop('checked', (wartosc == true));
        }
        return this[area][word][bit] = (wartosc == true);
    },
    /**
     * Zwraca slowo o zadanym adresie w postaci łancucha znakow 0 i 1
     * 
     * @param {Syting} adres
     * @returns {String}
     */
    readWord: function (adres) {
        var matches = adres.match(/^%(I|Q|M)W(\d+)\.(\d+)$/i); // dopasowanie adresu
        var area = matches[1].toUpperCase(); // obszar
        var word = matches[3]; // numer słowa
        return this[area][word].map(Number).join('');


    },
    /**
     * Zapisue wartosc do slowa o zadanym adresie
     * 
     * @param {type} adres
     * @param {String} wartosc w postaci 16-znakowe stringa skladajacego sie z 0 i 1
     * @returns {undefined}
     */

    wrtieWord: function (adres, wartosc) {
        var wartosc_array = wartosc.split(''); // rozbicie wartosci na pojedyncze "bity"
        var matches = adres.match(/^%(I|Q|M)W(\d+)\.(\d+)$/i); // dopasowanie adresu
        var area = matches[1].toUpperCase(); // obszar
        var word = matches[3]; // numer słowa
        for (var i = 0; i < this.arch; i++) { //ustawienie bitów w adresie w pętli
            this[area][word][i] = (wartosc_array[i] == true);
            if (area == 'M') { // w przypadku obszaro M od razu wpisujemy wartosci
                $('#MX' + word + '_' + i).prop('checked', (wartosc_array[i] == true)).change(); // zaznacza lub odznacza checkbox o danym adresie
            }
        }
    },
    /**
     * Wczytuje zawartość wejść do pamięci
     * 
     * @returns {undefined}
     */
    readInput: function () {
        for (var i = 0; i < this.memory; i++) {
            for (var j = 0; j < this.arch; j++) {
                var val = $('#IX' + i + '_' + j + ':checked').val(); //wartosc checkoboxa
                if (typeof val === 'undefined') { //checkbox nie jest zaznaczony
                    this.I[i][j] = false;
                } else { //checkbox jest zaznaczony
                    this.I[i][j] = true;
                }
            }
        }
    },
    writeOutput: function () {
        for (var i = 0; i < this.memory; i++) {
            for (var j = 0; j < this.arch; j++) {
                var previous = $('#QX' + i + '_' + j).prop('checked'); //poprzednia wartosc wejscia
                var current = this.Q[i][j]; //obecna
                if (previous !== current) { //wartosc sie zmienila? 
                    $('#QX' + i + '_' + j).prop('checked', current).change(); // zaznacza lub odznacza checkbox o danym adresie
                }
            }
        }

    }
};

var PLC_sim = {
    /**
     * @type Array Program sterownika
     */
    program: [],
    /**
     * @type Array stos adresów powrotnych, na wierzchu stosu przechowywany jest adres pod który wraca program po osiągnięciu adresu określonego przez callback_to_stack 
     */
    callback_from_stack: [],
    /**
     * 
     * @type Array stos adresów do powrotu, na wierzchu przechowywany jest adres po którego osiągnięciu program wraca do adresu określonego przez przez callback_from_stack
     */
    callback_to_stack: [],
    /**
     * 
     * @type Number Liczba szczebli w programie
     */
    rung_count: 0,
    /**
     * 
     * @type Array Tablica - indeksem jest numer szczebla a wartością liczna wierszy w danym szczeblu
     */
    row_count: [],
    /**
     * 
     * @type Number Liczba kolumn (instrukcji) na szczeblu
     */

    coulmn_count: 0,
    /**
     * 
     * @type Number Indeks aktualnego szczebla
     */
    current_rung: 0,
    /**
     * 
     * @type Number Indeksu aktualnego wiersza w danym szczeblu
     */
    current_row: 0,
    /**
     * 
     * @type Number Indeks danej kolumny w danym wierszu
     */
    current_column: 0,
    /**
     * 
     * @type Array Stan programu - tablica zawierajacy wartosci sygnałów. Null oznacza wartość jeszcze nie wyznaczona
     */
    state: [],
    /**
     * 
     * @type Array Poprzedni stan programu
     */
    state_previous: [],
    /**
     * 
     * @type Array Lista nazw fukcji które bede wywołowane co 0.1 sekundy
     */
    callback_0_1s: [],
    /**
     * Id timera 0.1 s
     * 
     * @type type
     */
    timer_0_1s: 0,
    /**
     * 
     * @type Boolean Określa czy właśnie został wykonany skok
     */
    jump: false,
    init: function () {
        //reset programu
        this.program = [];
        this.callback_from_stack = [];
        this.callback_to_stack = [];
        this.row_count = [];
        this.state = [];
        this.state_previous = [];
        this.current_rung = 0;
        this.current_row = 0;
        this.current_column = 0;

        this.rung_count = PLC_ladder_height();
        this.coulmn_count = PLC_ladder_width();
        PLC_mem.init();
    },
    /**
     * Zamienia liste elementów graficznych na listę instrukcji
     * 
     * 
     * @returns {undefined}
     */
    compile: function () {
        for (this.current_rung = 0; this.current_rung < this.rung_count; this.current_rung++) { // pętla po wszystkich szczeblach
            this.program.push([]); // dodanie nowego szczebla do programu
            this.state.push([]); // dodanie nowego szczebla do tablicy stanu programu
            var row_count = PLC_ladder_rung_height(PLC_get_element(this.current_rung, 0, 0)); // zliczenie ilości wierszów w szczeblu
            this.row_count.push(row_count);
            for (this.current_row = 0; this.current_row < row_count; this.current_row++) { // pętla po wszystkich wierszach w danym szczeblu
                this.program[this.current_rung].push([]); // dodanie nowego wiersza do aktualnego szczebla
                this.state[this.current_rung].push([]); // dodanie nowego wiersza do tablicy stanu programu
                for (this.current_column = 0; this.current_column < this.coulmn_count; this.current_column++) { // pętla po wszystkich kolumnach w danym wierszu - szczeblu
                    var element = PLC_get_element(this.current_rung, this.current_row, this.current_column); // określenie elementu
                    var element_class = PLC_get_element_class(element).toString(); // określenie klasy elementu
                    if (element_class.length === 0) { // przypadek szczególny - pusty element
                        element_class = 'PLC_empty';
                    }
                    this.state[this.current_rung][this.current_row].push(null); // wypełnienie stanów pustymi wartościami
                    var operands = []; // lista operandów dla danego elementu
                    var i = 0;
                    var operand;
                    while ((operand = element.attr('data-plc_operand_' + i)) !== undefined) { // tak długo jak mamy jakieś operandy
                        operands.push(operand);
                        i++;
                    }
                    this.program[this.current_rung][this.current_row].push({
                        class: element_class,
                        classes: element.classes(),
                        evaluate: window[element_class]['evaluate'],
                        rung: this.current_rung,
                        row: this.current_row,
                        column: this.current_column,
                        operands: operands
                    });
                    if (typeof window[element_class]['init'] !== "undefined") { //jeśli element posiada funkcje inicjalziacji
                        window[element_class]['init'](); //inicjalizacja elementu
                    }

                }
            }
        }
        this.state_previous = array_new_copy(this.state); //kopia stanu początkowego do stanu poprzedniego
        // wyzerowanie programu
        this.current_rung = 0;
        this.current_row = 0;
        this.current_column = 0;
    },
    /**
     * Czyści stan programu, ustawia wszystkie wartości na null
     */
    reset_state: function () {
        for (var rung = 0; rung < this.rung_count; rung++) {
            var row_count = this.state[rung].length;
            for (var row = 0; row < row_count; row++) {
                for (var column = 0; column < this.coulmn_count; column++) {
                    this.state[rung][row][column] = null;
                }
            }
        }
    },
    /**
     * Zwraca stan elementu o zadanej pozycji
     * 
     * @param {type} rung Szczebel
     * @param {type} row Wiersz
     * @param {type} column Kolumna
     * @returns {Boolean}
     */
    get_value: function (rung, row, column) {
        if (column < 0) { // szyna zasilająca, zawsze ma wartość true
            return true;
        } else {
            return this.state[rung][row][column];
        }
    },
    /**
     * Zwraca tablice operandów dla danej kombinacji wiersza szczebla i kolumny
     * @param {type} rung
     * @param {type} row
     * @param {type} column
     * @returns {PLC_sim@arr;@arr;@arr;program@pro;operands}
     */
    get_operands: function (rung, row, column) {
        return this.program[rung][row][column].operands;
    },
    get_current_operands: function () {
        return this.get_operands(this.current_rung, this.current_row, this.current_column);
    },
    /**
     * ustawia wartość dla elementu o zadanej pozycji
     * 
     * @param {type} rung Szczebel
     * @param {type} row Wiersz
     * @param {type} column Kolumna
     * @param {type} value wartość do ustawienia
     * @returns {undefined}
     */
    set_value: function (rung, row, column, value) {
        this.state[rung][row][column] = value;
        PLC_get_element(rung, row, column).removeClass("high low"); //usawa obecna reprezentacje stanu
        if (value > 0) { // jeśli stan wysoki 
            PLC_set_element_high(rung, row, column); // graficzna reprezentacja stanu wysokiego
        } else {
            PLC_set_element_low(rung, row, column);  // graficzna reprezentacja stanu niskiego
        }


    },
    /**
     * Ustawia wartość aktualnego elementu
     * 
     * @param {type} value wartośc do ustawienia
     * @returns {undefined}
     */
    set_current_value: function (value) {
        this.set_value(this.current_rung, this.current_row, this.current_column, value);
    },
    /**
     * Ustawia wartość elementu poniżej aktualnego
     * 
     * @param {type} value wartośc do ustawienia
     * @returns {undefined}
     */
    set_bottom_value: function (value) {
        this.set_value(this.current_rung, this.current_row + 1, this.current_column, value);
    },
    /**
     * Zwraca stan na wejściu z lewej strony aktualnego elemetu
     * @returns {Boolean}
     */
    get_input_left: function () {
        return this.get_value(this.current_rung, this.current_row, this.current_column - 1);
    },
    get_input_left_raising: function () {
        var current = this.get_input_left(); // obecna wartosc
        var previous = this.state_previous[this.current_rung][this.current_row][this.current_column - 1];
        if (current) {
            return !previous;
        } else {
            return false;
        }
    },
    /**
     * Zwraca stan na wejściu na dole aktualnego elementu
     */
    get_input_bottom: function () {
        return this.get_value(this.current_rung, this.current_row + 1, this.current_column);
    },
    /**
     * Zwraca stan na wejściu na górze aktualnego elementu
     */
    get_input_top: function () {
        return this.get_value(this.current_rung, this.current_row - 1, this.current_column);
    },
    /**
     * Zwraca klase obiektu znajdującego się po lewej stronie od aktualnego elementu
     * 
     * @returns {String}
     */
    get_class_left: function () {
        return this.program[this.current_rung][this.current_row][this.current_column - 1].class;
    },
    /**
     * Zwraca klase obiektu znajdującego się na dole po prawej stronie od aktualnego elementu
     * 
     * @returns {String}
     */
    get_class_bottom_right: function () {
        return this.program[this.current_rung][this.current_row + 1][this.current_column + 1].class;
    },
    /**
     * Zwraca klase obiektu znajdującego się na dole po lewej stronie od aktualnego elementu
     * 
     * @returns {String}
     */
    get_class_bottom_left: function () {
        return this.program[this.current_rung][this.current_row + 1][this.current_column - 1].class;
    },
    /**
     * Przeskakuje do pierwszej kolumny wiersza to_row.
     * Po osiągnięciu danego wiersza i kolumny powraca do instrukcji która wykonała skok.
     * 
     * @param {type} to_row
     * @param {type} to_column
     */
    jump_to_row: function (to_row, to_column) {
        this.callback_to_stack.push([to_row, to_column]); // ustawienie adresu DO którego program powróci
        this.callback_from_stack.push([this.current_row, this.current_column]); // ustawienie adresu Z którego program powróci

        // przejście do pierwszej kolumny nowego wiersza
        this.current_row = to_row;
        this.current_column = 0;
        this.jump = true;


    },
    /**
     * Funkcja wywoływana po osiągnięciu ostatnije instrukcji programu
     * Przesuwa wskaźnik na początek programu
     * Przepisuje obecny stan programo do poprzedniego stanu
     * Czyści obecny stan
     * Wczytuje wejścia
     * Czyści reprezentacje graficzna
     * 
     * @returns {undefined}
     */
    end: function () {
        PLC_mem.writeOutput(); // przepisuje zawartosc obliczonych wejsc
        this.current_rung = 0; // skok do pierwszego  szczebla
        this.current_row = 0; // powrót do pierwszego wiersza
        this.current_column = 0; // powrót do pierwszej kolumny
        this.state_previous = array_new_copy(this.state); // kopiuje stan programu do poprzedniego stanu programu
        this.reset_state(); // wyczysczenie stanu
        $(".PLC_column").removeClass("high low"); // wyczyszczenie graficznej reprezentacji stanów
        PLC_mem.readInput(); // wczytuje zawartosc wejsc do pamieci
    },
    /**
     * Funkcja wywolywana co 0.1 sekundy
     * 
     * @returns {undefined}
     */
    tick_0_1s: function () {
        for (var i = 0; i < PLC_sim.callback_0_1s.length; i++) {
            window[PLC_sim.callback_0_1s[i]](); // wywołuje funkcje z listy funkcji do wywołania
        }
    },
    /**
     * Wykonuje krok w symulacji
     * 
     * @returns {undefined}
     */
    step: function () {
        if (this.current_rung < this.rung_count) { //nie osiągneliśmy jee końca programu (ostatniegos szczebla)
            if (this.current_row < this.row_count[this.current_rung]) { // nie jesteśmy w ostatnim wierszu w danym szczeblu
                if (this.current_column < this.coulmn_count) { // nie jesteśmy w ostatniej kolumnie
                    PLC_set_element_selected(this.current_rung, this.current_row, this.current_column); // podświetlenie aktyalnej instrukcji
                    var instruction = this.program[this.current_rung][this.current_row][this.current_column]; // pobranie z programu instrukcji
                    var stan = instruction.evaluate(); // wykonanie instrukcji,
                    this.set_current_value(stan); // wykonanie instrukcji, zapisanie stanu w danej pozycji
                    var callback_length = this.callback_to_stack.length; // ilośc adresów na stosie do powrotu
                    // sprawdzamy czy mamy jakieś adresy i osiągneliśmy adres docelowy dla skoku
                    if (callback_length > 0 && (this.callback_to_stack[callback_length - 1][0] === this.current_row && this.callback_to_stack[callback_length - 1][1] === this.current_column)) {
                        this.callback_to_stack.pop(); // usunięcie adresu ze stosu adresów docelowych
                        var ret = this.callback_from_stack.pop(); // pobranie i usunięcie adresu ze ztosu adresów powrotnych
                        //powrót do wiersza i kolumny
                        this.current_row = ret[0];
                        this.current_column = ret[1];
                    } else { // jeśli nie osiągneliśmy adresu docelowego dla powrotu kontynuujemy normalne wykoannie programu
                        if (!this.jump) { // jeśli nie wykonujemy skoku  
                            this.current_column += 1; // przejście do nastepnej kolumny
                        } else { // jeśli właśnie wykonaliśmy skok nie zwiększamy nie ma potrzeby przechodzenia do następnej kolumny ponieważ instrukcja skoku sama ustawia kolumne
                            this.jump = false;
                        }
                    }
                    return true;
                } else { //jesteśmy w ostatniej kolumnie w danym wierszu
                    this.current_row += 1; // skok do nastepnego wiersza
                    this.current_column = 0; // powrót do pierwszej kolumny
                    return this.step(); // wywołanie instrukcji w nowym wierszu
                }
            } else { // jesteśmy w ostatnim wierszu w danym szczeblu
                this.current_rung += 1; // skok do nastepmego szczebla
                this.current_row = 0; // powrót do pierwszego wiersza
                this.current_column = 0; // powrót do pierwszej kolumny
                return this.step(); // wywołanie instrukcji w nowym szczeblu
            }
        } else { // jesteśmy na końcu ostatniego szczebla
            this.end(); // przepisanie wejścia/wyjścia
            return this.step(); // wywołanie instrukcji w pierwszym szczeblu 
        }
    }
};

function sim_start() {
    // ukrywa pasek edycji
    $('#PLC_edit_ui').hide('fast');

    //pokazuje sekcje we/wy
    $('#PLC_io').show('fast');

    //dodaje podpisy do we/wy
    $('#PLC_io label').tooltip({title: function () {
            var label_for = this.getAttribute('for');
            var address = '%' + label_for.replace('_', '.');
            // istnieje etykietka dla danego adresu
            if (PLC_labels.has_label(address)) {
                return PLC_labels.get_label(address);
            } else {
                return '';
            }
    }});

    //deaktywuje przycisk symulacji
    $('#PLC_start').addClass('disabled');
    $('#PLC_start').prop('disabled', true);

    // przygotowuje symulacje
    PLC_sim.init();
    PLC_sim.compile();
    PLC_sim.reset_state();

    //uaktywnia przycisk kroku
    $('#PLC_step').removeClass('disabled');
    $('#PLC_step').prop('disabled', false);

    //uaktywnia i zaznacza przycisk pauzy
    $('#PLC_pause').removeClass('disabled');
    $('#PLC_pause').prop('disabled', false);
    $('#PLC_pause').addClass('active');

    //uaktywnia przycisk startu porgramu
    $('#PLC_run').removeClass('disabled');
    $('#PLC_run').prop('disabled', false);

    //uaktywnia przycisk zatrzymianiu porgramu
    $('#PLC_stop').removeClass('disabled');
    $('#PLC_stop').prop('disabled', false);

    //uaktywnia przycisk pojedynczego uruchomienia
    $('#PLC_run_once').removeClass('disabled');
    $('#PLC_run_once').prop('disabled', false);

    simStarted = true;
    PLC_sim.timer_0_1s = setInterval(PLC_sim.tick_0_1s, 100);
}

function sim_step() {
    PLC_sim.step();
}



/**
 * Wykonuje symulacje w petli tak długo jak wartosc simLoop jest true
 * 
 * @returns {undefined}
 */
function sim_run_step() {
    PLC_sim.step();
    if (simLoop) {
        setTimeout(sim_run_step, 0);
    }
}

function sim_run_once() {
    PLC_sim.step();
    sim_run_once_step();
}

function sim_run_once_step() {
    // sprawdza czy nie osiągneliśmy końca programu
    if (!((PLC_sim.current_rung === PLC_sim.rung_count - 1) && (PLC_sim.current_row === PLC_sim.row_count[PLC_sim.current_rung] - 1) && (PLC_sim.current_column === PLC_sim.coulmn_count))) {
        PLC_sim.step();
        setTimeout(sim_run_once_step, 0);
    } else { // przepisujemy pamiec na wyjscia
        PLC_mem.writeOutput();
    }
}

/**
 * Uruchamia symulacje
 * 
 * @returns {undefined}
 */
function sim_run() {
    //odznacza przycisk pauzy
    $('#PLC_pause').removeClass('active');
    $('#PLC_run').addClass('active');
    simLoop = true;
    // główna pętla symulacji
    sim_run_step();
}

function sim_stop() {

    //zatrzymuje symulacje
    clearInterval(PLC_sim.timer_0_1s);
    simLoop = false;
    simstared = false;

    $('.PLC_column').removeClass("high low");
    $('#PLC_step').addClass('disabled');
    $('#PLC_step').prop('disabled', true);



    //deaktywuje i  przycisk pauzy
    $('#PLC_pause').addClass('disabled');
    $('#PLC_pause').prop('disabled', true);
    $('#PLC_pause').removeClass('active');

    //deaktywuje przycisk startu porgramu
    $('#PLC_run').removeClass('active');
    $('#PLC_run').addClass('disabled');
    $('#PLC_run').prop('disabled', true);

    //deaktywuje przycisk zatrzymianiu porgramu
    $('#PLC_stop').addClass('disabled');
    $('#PLC_stop').prop('disabled', true);

    //deaktywuje przycisk pojedynczego uruchomienia
    $('#PLC_run_once').addClass('disabled');
    $('#PLC_run_once').prop('disabled', true);

    //aktwuje przycisk startu symulacji
    $('#PLC_start').removeClass('disabled');
    $('#PLC_start').prop('disabled', false);
    PLC_set_element_selected(0, 0, 0);

    // aktywuje pasek edycji
    $('#PLC_edit_ui').show('fast');

    //usuwa podpisy dla we/wy
    $('#PLC_io label').tooltip('destroy');

    //ukrywa podglad i/o
    $('#PLC_io').hide('fast');


}

function sim_pause() {
    simLoop = false;
    $('#PLC_run').removeClass('active');
    $('#PLC_pause').addClass('active');
}