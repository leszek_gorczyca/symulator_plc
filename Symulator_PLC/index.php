<?php
require 'elements/elements.class.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Symulator PLC</title>

        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
        <script src="3rd_party/jquery-ui.js"></script>
        <link rel="stylesheet" href="3rd_party/jquery-ui.css">

        <script src="3rd_party/plugins.js"></script>

        <link rel="stylesheet" type="text/css" href="css/main.css">
        <script src="js/main.js"></script>
        <script src="js/sim.js"></script>
        <script src="js/raspberry.js"></script>
        <script src="elements/elements.php?content=js"></script>
        <script src="elements/helper.js"></script>

        <link rel="stylesheet" type="text/css" href="elements/elements.php?content=css">

        <!-- Ikony -->
        <link rel="stylesheet" type="text/css" href="3rd_party/css/font-awesome.css">


        <!-- bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="box">
            <header class="row header">
                <?php require 'inc/toolbar.php'; ?>
            </header>
            <div id="PLC_main" class="row content">
                <?php require 'inc/ladder.php'; ?>
            </div>
            <div class="row footer">
                <?php require 'inc/io.php'; ?> 
            </div>
            <footer class="row footer">
                <?php require 'inc/footer.php'; ?>         
            </footer>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="PLC_autor">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Praca dyplomowa inżynierska</h4>
                    </div>
                    <div class="modal-body">
                        <p style="text-align: center; font-size: large">
                        <author>Leszek Gorczyca</author><br>
                        <span>Projekt symulatora sterownika PLC z wykorzystanie technologii internetowych</span><br>
                        <span>2015/2106</span><br>
                        <a href="Instrukcja.pdf" target="_blank">Instrukcja obsługi</a>
                    </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="PLC_element_modal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form>



                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                        <button type="button" onclick="PLC_update_element()" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="PLC_save">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Zapisz</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="PLC_zapisz_name">Nazwa programu</label>
                                <input type="text" id="PLC_zapisz_name" class="form-control" value="plc_sim">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                        <button type="button" onclick="PLC_zapisz()" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="PLC_load">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Wczytaj</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="PLC_wczytaj_name">Nazwa programu</label>
                                <select  id="PLC_wczytaj_name" class="form-control">
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                        <button type="button" onclick="PLC_wczytaj()" class="btn btn-primary">Wczytaj</button>
                    </div>
                </div>
            </div>
        </div>
        <?php require 'inc/raspberry_conf.php'; ?>
        <?php require 'inc/etykiety.php'; ?>

    </body>
</html>
